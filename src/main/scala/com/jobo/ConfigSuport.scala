package com.jobo

import com.typesafe.config.ConfigFactory

trait ConfigSupport {
  lazy val config = ConfigFactory.load()
}
