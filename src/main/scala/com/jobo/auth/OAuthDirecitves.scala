package com.jobo.auth

import akka.http.scaladsl.model.{HttpRequest, HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.http.scaladsl.util.FastFuture
import akka.http.scaladsl.util.FastFuture._

import scala.concurrent.Future

trait OAuthDirectives extends OAuth1Protocol with OAuth1 {
  type TokenOpt = Option[Map[String, String]]

  private def authDirective(future: Future[OAuthResponse]): Directive1[OAuthResponse] = {
    onSuccess(future).flatMap(r => provide(r))
  }

  def authenticate: Directive1[OAuthResponse] = {
    val oAuthResponseF = requestToken
    authDirective(oAuthResponseF)
  }

  def oauthCallback(f: (String, String) => TokenOpt): Directive1[OAuthResponse] = {
    oauthCallbackAsync((t, v) => FastFuture.successful(f(t, v)))
  }

  def oauthCallbackAsync(f: (String, String) => Future[TokenOpt]): Directive1[OAuthResponse] = {
    parameters('oauth_token, 'oauth_verifier).tflatMap { extractedTuple =>
      val tokenF = f(extractedTuple._1, extractedTuple._2)
      val future = tokenF.fast.flatMap {
        case Some(tokens) => accessToken(tokens)
        case None         => Future.successful(AuthenticationFailed(HttpResponse(StatusCodes.BadRequest)))
      }
      authDirective(future)
    }
  }

  implicit class HttpRequestAuthentication(httpRequest: HttpRequest) {
    def addAuthentication(host: Option[String] = None, keys: Option[(String, String)] = None): HttpRequest =
      authenticateRequest(httpRequest, host, keys)
  }
}
