package com.jobo.auth

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.headers.{Authorization, GenericHttpCredentials, Location, RawHeader}
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Keep, RunnableGraph, Sink, Source}
import akka.util.ByteString
import com.typesafe.config.Config

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Random, Try}

trait OAuth1 extends OAuth1Base {
  import akka.http.scaladsl.model.StatusCodes._

  implicit def system: ActorSystem
  implicit def materializer: ActorMaterializer
  implicit def executor: ExecutionContext
  val config: Config

  val AuthenticationUri: String = "https://api.twitter.com/oauth/authorize"
  val RequestTokenUri: String = "https://api.twitter.com/oauth/request_token"
  val AccessTokenUri: String = "https://api.twitter.com/oauth/access_token"

  val key = config.getString("twitter.apiKey")
  val secret = config.getString("twitter.apiSecret")
  val token = config.getString("twitter.accessToken")
  val tokenSecret = config.getString("twitter.accessTokenSecret")

  def accessToken(params: Map[String, String]): Future[OAuthResponse] = {
    val request: HttpRequest           = httpRequestForAccessToken(params)
    val response: Future[HttpResponse] = Http().singleRequest(request)

    response.flatMap {
      case hr @ HttpResponse(OK, _, entity, _) => {
        val entitySource = entity.dataBytes
        val flow: Flow[ByteString, OAuthResponse, _] = Flow[ByteString].map(data => {
          val responseTokenOpt = parseResponseTokens(data)
          val oAuthResponseOpt = for {
            tokens: Map[String, String] <- responseTokenOpt
            _: String                   <- tokens.get(TokenM)
            _: String                   <- tokens.get(TokenSecret)
          } yield AccessTokenSuccess(tokens)
          oAuthResponseOpt.getOrElse(AuthenticationFailed(hr))
        })

        runGraph(entitySource, flow)
      }
      case hr => {
        Future.successful(AuthenticationFailed(hr))
      }
    }
  }

  def requestToken: Future[OAuthResponse] = {
    val request: HttpRequest = httpRequestForRequestToken
    val response: Future[HttpResponse] = Http().singleRequest(request)

    response.flatMap {
      case hr @ HttpResponse(OK, _, entity, _) => {
        val entitySource: Source[ByteString, _] = entity.dataBytes
        val flow: Flow[ByteString, OAuthResponse, _] = Flow[ByteString].map(data => {
          val responseTokenOpt = parseResponseTokens(data)
          responseTokenOpt
            .flatMap(tokens => requestToken2OauthResponse(tokens, hr))
            .getOrElse(TokenFailed(hr))
        })
        runGraph(entitySource, flow)

      }
      case hr => {
        Future.successful(AuthenticationFailed(hr))
      }
    }
  }

  // TODO: Add second method for direct calls'

  def authenticateRequest(request: HttpRequest,
                           host: Option[String] = None,
                           keys: Option[(String, String)] = Some(token, tokenSecret)): HttpRequest = {
    val params = AuthenticationHeader(
      request.method.value,
      "https://stream.twitter.com/1.1/statuses/filter.json",
//      host.map { h =>
//        request
//          .uri
//          .withScheme("https")
//          .withHost(h)
//      }.getOrElse(request.uri).toString.split('?')(0),
      key,
      secret,
      Some(token, tokenSecret)
    )
    val headerParamsForRequest = headerParams(params)
    request.withHeaders(Authorization(GenericHttpCredentials("OAuth", headerParamsForRequest)))
  }

  private def runGraph(source: Source[ByteString, _],
                       flow: Flow[ByteString, OAuthResponse, _]): Future[OAuthResponse] = {
    val graph: RunnableGraph[Future[OAuthResponse]] =
      source.via(flow).toMat(Sink.head[OAuthResponse])(Keep.right)
    graph.run()
  }

  private def httpRequestForAccessToken(params: Map[String, String]): HttpRequest = {
    HttpRequest(
      method = HttpMethods.POST,
      uri = AccessTokenUri
    ).withHeaders(Authorization(GenericHttpCredentials("OAuth", params)))
  }

  private def requestToken2OauthResponse(tokens: Map[String, String],
                                         hr: HttpResponse): Option[OAuthResponse] =
    for {
      isCallbackConfirmed: String <- tokens.get(CallbackConfirmed)
      oauthToken: String          <- tokens.get(TokenM)
    } yield {
      if (isCallbackConfirmed == "true") {
        val redirectUriWithParam = s"$AuthenticationUri?$TokenM=$oauthToken"
        val redirectResponse = HttpResponse(status = Found).withHeaders(Location(redirectUriWithParam))
        RedirectionSuccess(redirectResponse, tokens)
      } else {
        TokenFailed(hr)
      }
    }

  private def parseResponseTokens(data: ByteString): Option[Map[String, String]] =
    Try {
      val result = data.utf8String.split("&").toList
      result
        .map(pair => {
          val arr = pair.split("=")
          arr(0) -> arr(1)
        })
        .toMap[String, String]
    }.toOption

  private def httpRequestForRequestToken: HttpRequest = {
    val httpMethod: HttpMethod = HttpMethods.POST
    val authenticationHeader = AuthenticationHeader(
      httpMethod.value,
      RequestTokenUri,
      key,
      secret
    )
    HttpRequest(
      method = httpMethod,
      uri = RequestTokenUri
    ).withHeaders(Authorization(GenericHttpCredentials("OAuth", headerParams(authenticationHeader))))
  }
}

trait OAuth1Base extends OAuth1Protocol with Signature {

  val random = new Random(System.currentTimeMillis())

  def headerParams(header: AuthenticationHeader): Map[String, String] = {

    def timestamp: String =
      (System.currentTimeMillis() / 1000L).toString

    def nonce: String =
      random.alphanumeric.take(32).mkString

    def paramsMap = Map (
      ConsumerKey     -> header.consumerKey,
      Nonce           -> nonce,
      SignatureMethod -> HmacSHA1,
      Timestamp       -> timestamp,
      Version         -> Version1
    )

    val params: Map[String, String] = header.tokenOpt.map { case (token, _) =>
      paramsMap + (TokenM -> token)
    }.getOrElse(paramsMap)

    params.get(Timestamp).foreach(println _)

    val signature = generate(
      header.httpMethod,
      header.uri,
      params.toList,
      header.consumerSecret,
      header.tokenOpt.map(_._2)
    )

    params + (Signature -> signature)
  }

}

trait OAuth1Protocol {
  val ConsumerKey       = "oauth_consumer_key"
  val Nonce             = "oauth_nonce"
  val Signature         = "oauth_signature"
  val SignatureMethod   = "oauth_signature_method"
  val Timestamp         = "oauth_timestamp"
  val Version           = "oauth_version"

  val CallbackConfirmed = "oauth_callback_confirmed"
  val Verifier          = "oauth_verifier"
  val TokenM            = "oauth_token"
  val TokenSecret       = "oauth_token_secret"

  val HmacSHA1 = "HMAC-SHA1"
  val Version1 = "1.0"

  final case class AuthenticationHeader(
    httpMethod: String,
    uri: String,
    consumerKey: String,
    consumerSecret: String,
    tokenOpt: Option[(String, String)] = None
  )

  final case class OAuthInfo(
    consumerKey: String,
    consumerSecret: String,
    requestTokenUri: String,
    accessTokenUri: String,
    authenticationUri: String
  )

  sealed trait OAuthResponse
  sealed trait OAuthSuccessfulResponse extends OAuthResponse
  sealed trait OAuthFailedResponse     extends OAuthResponse

  final case class RedirectionSuccess(httpResponse: HttpResponse, tokens: Map[String, String])
    extends OAuthSuccessfulResponse
  final case class AccessTokenSuccess(tokens: Map[String, String]) extends OAuthSuccessfulResponse

  final case class CallbackFailed(httpResponse: HttpResponse)       extends OAuthFailedResponse
  final case class AuthenticationFailed(httpResponse: HttpResponse) extends OAuthFailedResponse
  final case class TokenFailed(httpResponse: HttpResponse)          extends OAuthFailedResponse
}

trait Signature {
  def generate(httpMethod: String,
               uri: String,
               params: List[(String, String)],
               consumerSecret: String,
               oAuthTokenSecret: Option[String] = None): String = {
    val np = normalizeParams(params)
    val sk = s"${encode(consumerSecret)}&${oAuthTokenSecret.getOrElse("")}"
    val bs = s"${httpMethod.toUpperCase}&${encode(uri)}&$np"
    encode(hmac(sk, bs))
  }

  private def normalizeParams(params: List[(String, String)]): String = {
    params.sortBy(identity).map { case (a, b) =>
      s"${encode(a)}%3D${encode(b)}"
    }.mkString("%26")
  }

  private def hmac(key: String, baseString: String, algorithm: MACAlgorithm = SHA1): String = {
    import java.util.Base64
    import javax.crypto.Mac
    import javax.crypto.spec.SecretKeySpec

    val alg = algorithm.value
    val sks = new SecretKeySpec(key.getBytes, alg)
    val mac = Mac.getInstance(alg)

    mac init sks

    val calc = mac.doFinal(baseString.getBytes)
    val base64Val = Base64.getEncoder.encode(calc)
    new String(base64Val, "UTF-8")
  }

  private def encode(input: String): String =
    input.map(c => ReservedCharEncoding.getOrElse(c,c)).mkString

  private val ReservedCharEncoding: Map[Char, String] =
    Map(
      '!'  -> "%21",
      '#'  -> "%23",
      '$'  -> "%24",
      '&'  -> "%26",
      '\'' -> "%27",
      '('  -> "%28",
      ')'  -> "%29",
      '*'  -> "%2A",
      '+'  -> "%2B",
      ','  -> "%2C",
      '/'  -> "%2F",
      ':'  -> "%3A",
      ';'  -> "%3B",
      '='  -> "%3D",
      '?'  -> "%3F",
      '@'  -> "%40",
      '['  -> "%5B",
      ']'  -> "%5D"
    )

  sealed trait MACAlgorithm {
    def value: String
  }

  final case object SHA1 extends MACAlgorithm {
    val value = "HmacSHA1"
  }

  final case object MD5 extends MACAlgorithm {
    val value = "HmacMD5"
  }
}