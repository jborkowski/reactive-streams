package com.jobo
import akka.NotUsed
import akka.actor.{ActorSystem, Terminated}
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.headers.Authorization
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.scaladsl.{Framing, Source}
import akka.stream.{ActorMaterializer, Materializer}
import akka.util.ByteString
import com.jobo.auth.{OAuth1, OAuthDirectives}
import io.circe.Json

import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.concurrent.duration._
import scala.util.Success


object TestApp extends App with ConfigSupport with OAuth1 with OAuthDirectives {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executor = system.dispatcher
 // val logger = Logging(system, getClass)

//  def res = Http().singleRequest(
//    HttpRequest(uri = Uri("https://api.twitter.com/1.1/statuses/user_timeline.json")).addAuthentication(None)
//  )
//  val r = Await.result(res, 3 minutes)
//  r.entity.toStrict(10 seconds).onComplete(t => println(t))

  //val tokens = Await.result(requestToken.map { case RedirectionSuccess(_, t) => t }, 10 seconds)

  val connectionFlow = Http().cachedHostConnectionPoolHttps[Long]("stream.twitter.com")

  val params = Map(
    "track" -> "twitter",
    "follow" -> "",
    "locations" -> ""
  ).filter(_._2.nonEmpty)

  val request: HttpRequest = HttpRequest(uri = Uri("/1.1/statuses/filter.json").withQuery(Uri.Query(params)))
    .withMethod(HttpMethods.POST).addAuthentication(host = Some("stream.twitter.com"))

  Source
    .single(request -> 1L)
    .via(connectionFlow)
    .map {
      case (Success(response), _) =>
        response.entity.dataBytes
      case e =>
        println(e)
        Source.empty[ByteString]
    }
    .flatMapConcat(identity)
    .via(Framing.delimiter(ByteString("\n"), maximumFrameLength = 1000, allowTruncation = true))
    .map { byteString =>
      val json = Json.fromString(byteString.utf8String)
      json
    }
    .runForeach(i => println(i.toString()))
    .onComplete { tr =>
      val terminate: Future[Terminated] = system.terminate()
      Await.result(terminate, 10 seconds)
    }

}