name := "reactive-streams"

version := "1.0"

organization := "com.jobo"

scalaVersion := "2.12.2"

resolvers += Resolver.bintrayRepo("hseeberger", "maven")

libraryDependencies ++= {
  val akkaVersion       = "2.5.3"
  val akkaHttpVersion   = "10.0.8"
  val scalaTestVersion  = "3.0.1"
  val slickVersion      = "3.2.0"
  val circeVersion      = "0.8.0"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % "test",
    "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test,
    "org.scalatest"     %% "scalatest" % scalaTestVersion % "test",
    "org.scalamock"     %% "scalamock-scalatest-support" % "3.5.0" % Test,
    "de.heikoseeberger" %% "akka-http-circe" % "1.17.0",

    "pl.iterators" %% "kebs-slick" % "1.4.3",
    "pl.iterators" %% "kebs-spray-json" % "1.4.3",
    "pl.iterators" %% "kebs-akka-http" % "1.4.3",

    "org.slf4j" % "slf4j-nop" % "1.6.4",
    "org.flywaydb" % "flyway-core" % "3.2.1",
    "org.postgresql" % "postgresql" % "9.4-1201-jdbc41",
    "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
    "com.typesafe.slick" %% "slick" % slickVersion
  ) ++ Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % circeVersion)
}